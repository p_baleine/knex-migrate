exports.up = function(knex) {
  return knex.schema.createTable('posts', function(t) {
    t.increments().primary();
    t.string('title');
    t.text('text');
    t.timestamps();
  });
};

exports.down = function(knex) {
};
