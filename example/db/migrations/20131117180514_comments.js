var when = require('when');

exports.up = function(knex) {
  return when.all([
    knex.schema.createTable('comments', function(t) {
      t.increments().primary();
      t.integer('post_id').notNull().references('id').inTable('posts');
      t.string('title');
      t.text('text');
      t.timestamps();
    }),

    knex.schema.createTable('tags', function(t) {
      t.increments().primary();
      t.integer('post_id').notNull().references('id').inTable('posts');
      t.string('name');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex) {
};
