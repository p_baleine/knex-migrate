var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    when = require('when'),
    migration = require('../lib/migration'),
    Migration = migration.Migration,
    expect = chai.expect;

chai.use(sinonChai);

describe('migration', function() {
  beforeEach(function() {
    this.def = { up: function() {}, down: function() {} };
    sinon.spy(migration, 'Migration');
  });

  afterEach(function() {
    migration.Migration.restore();
  });

  it('should instanciate Migration obj.', function() {
    expect(migration(this.def, 'asc')).to.be.an.instanceOf(Migration);
    expect(migration.Migration.lastCall.args[0]).to.have.property('up', this.def.up);
    expect(migration.Migration.lastCall.args[0]).to.have.property('down', this.def.down);
  });

  describe('when order is specified as `asc`', function() {
    it('should instanciate Migration obj with task as `up`', function() {
      migration(this.def, 'asc');

      expect(migration.Migration.lastCall.args[0]).to.have.property('task', 'up');
    });
  });

  describe('when order is specified as `desc`', function() {
    it('should instanciate Migration obj with task as `down`', function() {
      migration(this.def, 'desc');

      expect(migration.Migration.lastCall.args[0]).to.have.property('task', 'down');
    });
  });

  describe('Migration', function() {
    beforeEach(function() {
      this.name = '20131119_create_posts';
    });

    describe('on getting `promise`', function() {
      describe('when task is specified as `up`', function() {
        describe('when `up` task is specified on instanciation', function() {
          beforeEach(function() {
            this.upDeferred = when.defer();
            this.upSpy = sinon.stub().returns(this.upDeferred.promise);;
            this.obj = new Migration({ up: this.upSpy, task: 'up', name: this.name });
          });

          it('should execute passed `up` task.', function() {
            this.obj.promise;

            expect(this.upSpy.called).to.be.ok;
          });

          it('should return promise.', function() {
            // TODO more test for promise interface.
            expect(this.obj.promise).to.have.property('then');
          });

          it('should emit `up` with migration name.', function() {
            var spy = sinon.spy(),
                _this = this;

            this.obj.on('up', spy);
            this.upDeferred.resolve();

            this.obj.promise.then(function() {
              expect(spy).to.have.been.calledWith(_this.name);
            });
            ;
          });
        });

        describe('when `up` task is not specified on instanciation', function() {
          it('should throw Error.');
        });
      });

      describe('when task is specified as `desc`', function() {
        describe('when `down` task is specified on instanciation', function() {
          it('should execute passed `down` task.');
          it('should emit `down` with migration name.');
        });

        describe('when `down` task is not specified on instanciation', function() {
          it('should throw Error.');
        });
      });
    });
  });
});
