
var runner = new Runner(config),
    versions = new VersionManager(config);

runner
  .series(versions.to(versions.latestVersion()))
  .then(cb);

// ...

if (!versions.isValid('some version')) {
  throw new Error('invalid version.');
}

runner
  .series(versions.to('some version'))
  .then(cb);

// plus versions.currentVersion()

// Integration tests are executed on `Runner`.

// Unit test of Migration object ensures
// latestVersion, currentVersion and isValid interfaces
