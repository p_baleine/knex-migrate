var expect = require('chai').expect,
    fs = require('fs'),
    resolve = require('path').resolve,
    _ = require('underscore'),
    moment = require('moment'),
    config = require('../lib/config'),
    Config = config.Config;

var testConfig = {
  migrations: './db/migrations',
  db: {
    client: 'sqlite3',
    connection: {
      filename: './db/blog-dev.db'
    }
  }
};

var path = './config.json';

describe('config', function() {

  before(function(done) {
    // create config file for test.
    fs.writeFile(path, JSON.stringify(testConfig), done);
  });

  after(function(done) {
    fs.unlink(path, done);
  });

  describe('when specified path exists', function() {
    it('should return Config object.', function() {
      expect(config(path)).to.be.instanceOf(Config);
    });

    describe('Config\'s properties', function() {
      beforeEach(function() {
        this.obj = config(path);
      });

      it('should have migrations value.', function() {
        expect(this.obj).to.have.property('migrations', testConfig.migrations);
      });

      it('should have prefixFormat value.', function() {
        expect(this.obj).to.have.property('prefixFormat', testConfig.prefixFormat);
      });

      it('should have db value.', function() {
        expect(this.obj).to.have.property('db');
        expect(this.obj.db).to.have.property('client', testConfig.db.client);
        expect(this.obj.db).to.have.property('connection');
        expect(this.obj.db.connection)
          .to.have.property('filename', testConfig.db.connection.filename);
      });
    });
  });

  describe('Config', function() {
    beforeEach(function() {
      this.obj = new Config(_.extend({ prefixFormat: 'YYYYMMDDHHmm' }, testConfig));
    });

    describe('#migrationName', function() {
      it('should prefix passed name with timestamp and add `.js` extension', function() {
        expect(this.obj.migrationName('abc'))
          .to.equal(moment().format('YYYYMMDDHHmm') + '_abc.js');
      });
    });

    describe('#migrationsPath', function() {
      it('should return resolved migrations path', function() {
        expect(this.obj.migrationsPath()).to.equal(resolve(testConfig.migrations));
      });
    });
  });
});
