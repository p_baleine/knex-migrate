var _ = require('underscore'),
    inherits = require('util').inherits,
    EventEmitter = require('events').EventEmitter,
    knex = require('knex').client;

// Expose `migration`.

module.exports = exports = migration;

// Expose `Migration`.

exports.Migration = Migration;

/**
 * Create new Migration and return this.
 * 
 * @param {Object} def
 * @param {String} order
 * @return {Migration}
 */

function migration(def, order) {
  return new exports.Migration(_.extend({ task: order === 'asc' ? 'up' : 'down' }, def));
}

/**
 * Migration constructor.
 * Migration object have `promise` property which is promise of the object's migration.
 *
 * @param {Object} def
 *  - up : up function which should returns promise.
 *  - down : down function which should returns promise.
 */

function Migration(def) {
  EventEmitter.call(this);
  this.def = def;

  // validate promise interface
  if (!(typeof this.def[def.task] === 'function')) {
    throw new Error(def.task + ' is not implemented on ' + this.version);
  }

  this.__defineGetter__('promise', this[def.task].bind(this));
}

// Inherit `EventEmitter`.

inherits(Migration, EventEmitter);

/**
 * Execute up task.
 * Emit `up` event when task is done.
 */

Migration.prototype.up = function() {
  var _this = this;

  return this.def.up(knex).then(function() {
    _this.emit('up', _this.def.name);
  });
};

/**
 * Execute down task.
 * Emit `down` event when task is done.
 */

Migration.prototype.down = function() {
  return this.def.down(knex);
};
