var glob = require('glob'),
    series = require('when/sequence'),
    path = require('path'),
    migration = require('./migration'),
    resolve = path.resolve;

// Expose `migrate`.

module.exports = migrate;

/**
 * Execute migration
 */

function migrate(config, cb) {
  var order = 'asc', // asc or desc based on specified version.
      pattern = '*.js', // TODO create pattern based on config?
      migrationDefinitions = glob.sync(path.join(config.migrations, pattern)),
      tasks;
 
  if (migrationDefinitions.length === 0) { return cb(new Error('no migrations.')); }

  tasks = migrationDefinitions
    // TODO .sort(order)
    .map(function(def) {
      return function() {
        // TODO error handling
        def = resolve('.', def);
        return migration(require(def), order).promise;
      };
    });
 
  // process promises.
  return series(tasks).then(cb);
}

