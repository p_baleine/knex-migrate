var _ = require('underscore'),
    moment = require('moment'),
    path = require('path'),
    fs = require('fs'),
    resolve = path.resolve,
    exists = fs.existsSync;

// Default values.
var defaults = exports.defaults = {
  // path to store migration files
  migrations: './',

  // prefix format of generated migration file
  prefixFormat: 'YYYYMMDDHHmmss',

  // knex initialization params
  db: {}
};

// Expose `config`.

exports = module.exports = config;

// Expose `Config`.

exports.Config = Config;

/**
 * Load config from path.
 *
 * @param {String} path
 * @return {Config}
 */

function config(path) {
  var obj = load(path);
  return obj ? new Config(obj) : null;
}

/**
 * Config constructor.
 *
 * @param {object} obj
 */

function Config(obj) {
  _.extend(this, defaults, _.pick(obj, _.keys(defaults)));
}

/**
 * Return the migration file name based on
 * `baseName` and current time.
 *
 * @param {baseName} name
 * @return {String} migrationName
 */

Config.prototype.migrationName = function(baseName) {
  return [moment().format(this.prefixFormat), baseName].join('_') + '.js';
};

/**
 * Return the path that migration files are stored.
 *
 * @return {String} migrationsPath
 */

Config.prototype.migrationsPath = function() {
  return resolve(this.migrations);
};

/**
 * `require` the `path`
 */

function load(path) {
  path = resolve(path);
  if (!exists(path)) { return null; }
  return require(path);
}

