# knex-migrate

Promise based databse migration tool, build on Knex query builder.

## Installation

With node previously installed:

```bash
$ npm install -g knex-migrate
```

## Usage

Via `--help`:

```bash

  Usage: knex-migrate <command> [options]

  Options:

    -h, --help     output usage information
    -V, --version  output the version number

  Commands:

    init [path]         create config.json file
    run [name ...]      run migrations
    create <name>       create a migration

```

## Example

First, we need `config.json` file which describes configration for `knex-migrate`.

```bash
$ knex-migrate init ./db/config.json
prompt: migrations directory:  ./db/migrations
prompt: databse client:  sqlite3
prompt: database filename:  ./db/blog-dev.db
./db/config.jsonis created.

$ cat ./db/config.json
{
  "migrations": "./db/migrations",
  "db": {
    "client": "sqlite3",
    "connection": {
      "filename": "./db/blog-dev.db"
    },
    "debug": true
  }
}
```

Now we can create migration file:

```bash
$ knex-migrate create create_posts -c ./db/config.json

$ ls ./db/migrate
20120419084633_crete_posts.js

$ cat ./db/migrate
exports.up = function(knex) {
};

exports.down = function(knex) {
};
```

Then edit created migration file via `knex` apis.

```
$ cat ./db/migrate/20120419084633_crete_posts.js

exports.up = function(knex) {
  knex.schema.createTable('posts', function(t) {
    t.increments().primary();
    t.string('title');
    t.text('text');
    t.timestampes();
  });
};

exports.down = function(knex) {
};
```

and run the migration:

```bash
$ knex-migrate run -c ./db/config.json

$ sqlite3 ./db/blog-dev.db
sqlite> .schema
CREATE TABLE "posts" ("id" integer primary key autoincrement not null, "title" varchar(255) not null, "text" varchar(255) not null, "created_at" datetime not null, "updated_at" datetime);
```

output sample

```bash
==  CreatePosts: migrating ====================================================
-- create_table(:posts)
   -> 0.0029s
==  CreatePosts: migrated (0.0032s) ===========================================

==  CreateComments: migrating =================================================
-- create_table(:comments)
   -> 0.0029s
-- add_index(:comments, :post_id)
   -> 0.0012s
==  CreateComments: migrated (0.0046s) ========================================

==  CreateTags: migrating =====================================================
-- create_table(:tags)
   -> 0.0032s
-- add_index(:tags, :post_id)
   -> 0.0015s
==  CreateTags: migrated (0.0052s) ============================================

==  CreateUsers: migrating ====================================================
-- create_table(:users)
   -> 0.0032s
==  CreateUsers: migrated (0.0034s) ===========================================
```
